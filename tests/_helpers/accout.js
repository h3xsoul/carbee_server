const Account = require('../../src/models/account');
const TokenSotre = require('../../src/models/tokenStore');
const tokenModule = require('../../src/utils/token');

module.exports = {
  async createAccount() {
    const acc = await Account.create({});
    const tokenPare = tokenModule.issuePareTokens(acc.id);

    const tokenstore = await TokenSotre.create({
      account: acc.id,
      refreshToken: tokenPare.refreshToken
    });

    return {
      acc,
      tokenPare
    };
  }
};
