const jwt = require('jsonwebtoken');

module.exports = (data, options = {}) => jwt.sign(data, process.env.JWT_SECRET, options);
