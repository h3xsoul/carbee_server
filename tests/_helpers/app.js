const supertest = require('supertest');

const app = require('../../src/app');
const mongo = require('../../src/handlers/mongodb');
const { PORT } = require('../../src/config');

let server = {};

module.exports = {
  getRequest() {
    return supertest(server);
  },
  async start() {
    await mongo.connect();
    server = app.listen(PORT);
  },
  async stop() {
    await mongo.close();
    await server.close();
  },
  async dropDb() {
    await mongo.mongoose.connection.db.dropDatabase();
  }
};
