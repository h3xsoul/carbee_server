const { assert } = require('chai');
const App = require('./_helpers/app');
const issueToken = require('./_helpers/issueToken');
const Account = require('../src/models/account');

let app;
let account;
let authLine;

describe.skip('Account module testing', () => {
  before(async () => {
    await App.start();
    account = await Account.create({});
    authLine = `Bearer ${issueToken({ id: account.id })}`;
  });
  after(async () => {
    // await App.dropDb();
    await App.stop();
  });
  beforeEach(() => {
    app = App.getRequest();
  });

  it('Should successfuly get account info with valid token', async () => {
    const res = await app.get('/v1/acc').set('Authorization', authLine);

    assert.equal(res.status, 200);
    assert.isTrue(res.text === 'ok');
  });
});
