const { assert } = require('chai');
const App = require('./_helpers/app');
const issueToken = require('./_helpers/issueToken');
const accountHelper = require('./_helpers/accout');

let app;
let account;

const mocha = this;

describe('Authentication test', () => {
  describe('Local authentication', () => {
    before(async () => {
      await App.start();
      account = await accountHelper.createAccount();
    });
    after(async () => {
      await App.stop();
    });
    beforeEach(() => {
      app = App.getRequest();
    });

    it.skip('User can succesfully login', async () => {
      const res = await app.post('/v1/auth/local/login').send({
        phone: '12345678910',
        password: 'user'
      });

      assert.equal(res.status, 200);
      assert.isTrue(typeof res.body.token === 'string');
      assert.isTrue(typeof res.body.refreshToken === 'string');
    });
    it('User gets 403 on invalid credentials');
    it('User receives 401 on expired token', async () => {
      const expiredToken = issueToken({ id: 1 }, { expiresIn: '0ms' });
      const res = await app.get('/v1/acc').set('Authorization', `Bearer ${expiredToken}`);

      assert.equal(res.status, 401);
    });

    it('User can get new access token using refresh token', async () => {
      console.log();
      const res = await app.post('/v1/auth/refresh').send({
        account_id: account.acc.id,
        refreshToken: account.tokenPare.refreshToken
      });

      assert.equal(res.status, 200);
      assert.isTrue(typeof res.body.token === 'string');
      assert.isTrue(typeof res.body.refreshToken === 'string');
    }).timeout(3000);

    it('User can use refresh token only once');
    it('Refresh tokens become invalid on logout');
    it('Multiple refresh tokens are valid');
  });
});
