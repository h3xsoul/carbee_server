const mongo = require('../../src/handlers/mongodb');
const Account = require('../../src/models/account');
const Quote = require('../../src/models/quote');
const Customer = require('../../src/models/customer');

async function conn() {
  await mongo.connect();
  Customer.create(
    {
      name: 'A',
      cars: [{ make: 'Toyota', model: 'Corolla', year: '2004' }]
    },
    (err, customer) => {
      Account.create({ type: 'Customer', entity: customer.id }, (err, account) => {
        Quote.create(
          {
            customer: customer.id,
            car: customer.cars[0].id
          },
          (err, quote) => {
            console.log(quote);
          }
        );
      });
    }
  );
}

// conn();

async function popl() {
  await mongo.connect();
  try {
    const result = await Quote.findById('5bb37d623dc49066a2a79e67')
      .populate({
        path: 'customer',
        match: {}
      })
      .exec();
  } catch (e) {
    console.log(e);
  }

  console.log(result);
  mongo.close();
}

popl();
