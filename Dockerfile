FROM node:10.13-alpine

WORKDIR /app

COPY . .

RUN apk add --no-cache python make g++ \
  && npm install


EXPOSE 8081

CMD [ "npm", "run", "dev" ]
