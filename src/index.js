// const IO = require('socket.io');
const SocketService = require('./service/socket.service');
// const socketEvents = require('./utils/socketEvents');
const mongo = require('./handlers/mongodb');
const logger = require('./utils/logger');
const app = require('./app');

async function createApp() {
  await mongo.connect();

  const server = app.listen(8081, () => {
    logger.info('App at http://localhost:8081');
  });

  // var io = new IO(server); /* eslint-disable-line */
  // socketEvents(io);

  SocketService(server);

  // const io = new Socket(server, {
  //   path: '/socket-chat'
  // });

  // app.context.$socket = io;

  return app;
}

module.exports = createApp();
