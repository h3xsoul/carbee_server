const Message = require('../models/message');
const Chat = require('../models/chat');

module.exports = socket => {
  socket.on('Repairshop::newQuoteChat', async data => {
    let chat = await Chat.findOne({ quote: data.quote._id, repairshop: socket.userdata.id });
    if (chat) return console.log(`chat is init for ${socket.userdata.id}`);

    chat = await Chat.create({
      quote: data.quote._id,
      customer: data.quote.customer,
      repairshop: socket.userdata.id,
      price: data.price,
      hours: data.hours,
      mitutes: data.minutes
    });

    await chat.populate('repairshop', '_id profile').execPopulate();

    const message = await Message.create({
      chat: chat.id,
      customer: data.quote.customer,
      author: socket.userdata.id,
      type: socket.userdata.usertype,
      body: data.message
    });

    // await chat.populate('repairshop', 'profile').execPopulate();

    // await message.populate('author').execPopulate();

    socket.join(`chat${chat.id}`);
    socket.to(`Customer${data.quote.customer}`).emit('Customer::notificationNewChatQuote', { chat, message });
  });

  socket.on('Repairshop::joinChat', async chatId => {
    const chat = await Chat.find({ _id: chatId, repairshop: socket.userdata.id })
      .limit(1)
      .exec();
    if (!chat.length) return console.log(`Chat not found ${chatId}`);

    socket.join(`chat${chatId}`);
    console.log(`Repairshop joined to chat${chatId}`);
  });

  socket.on('Repairshop::sendMessage', async ({ chatId, newMessage }, cb) => {
    const chat = await Chat.findOne({ _id: chatId, repairshop: socket.userdata.id });
    if (!chat) return console.log(`Repairshop Chat not found ${chatId}`);

    const message = await Message.create({
      chat: chatId,
      author: socket.userdata.id,
      type: socket.userdata.usertype,
      body: newMessage
    });

    // await chat.populate('repairshop', 'profile').execPopulate();

    // await message.populate('author').execPopulate();

    socket.to(`chat${chatId}`).emit(`newMessage${chatId}`, message);
    cb(message);
  });
};
