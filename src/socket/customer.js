const Message = require('../models/message');
const Chat = require('../models/chat');

module.exports = socket => {
  socket.on('ctmr::newMessage', () => {});

  socket.on('Customer::joinChat', async chatId => {
    const chat = await Chat.findOne({ _id: chatId, customer: socket.userdata.id });
    if (!chat) return console.log(`Chat not found ${chatId}`);
    socket.join(`chat${chat.id}`);
    console.log(`Customer joined to chat${chat.id}`);
  });

  socket.on('Customer::sendMessage', async ({ chatId, newMessage }, cb) => {
    const chat = await Chat.findOne({ _id: chatId, customer: socket.userdata.id });
    if (!chat) return console.log(`Customer Chat not found ${chatId}`);

    const message = await Message.create({
      chat: chatId,
      author: socket.userdata.id,
      type: socket.userdata.usertype,
      body: newMessage
    });

    socket.to(`chat${chatId}`).emit(`newMessage${chatId}`, message);
    cb(message);
  });
};
