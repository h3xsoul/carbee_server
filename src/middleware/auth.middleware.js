const TokenService = require('../service/token.service');

module.exports = async (ctx, next) => {
  const { header } = ctx.request;

  if (!header.authorization) return ctx.throw(401);

  const parts = ctx.header.authorization.split(' ');

  const scheme = parts[0];
  if (!parts.length === 2 || !/^Bearer$/i.test(scheme))
    return ctx.throw(401, 'Bad Authorization header format. Format is "Authorization: Bearer <token>"');

  const credentials = parts[1];

  const decode = await TokenService.verify(credentials).catch(e => {});
  if (!decode) return ctx.throw(401);

  ctx.state.user = {
    rjwt: decode.rjwt,
    ...decode.dataToken
  };
  await next();
};
