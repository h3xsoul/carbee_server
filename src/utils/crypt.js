const bcrypt = require('bcryptjs');

module.exports = {
  generateHash(password) {
    return bcrypt.genSalt(10).then(salt => bcrypt.hash(password, salt));
  },
  compare(password, hashedPassword) {
    return bcrypt.compare(password, hashedPassword);
  }
};
