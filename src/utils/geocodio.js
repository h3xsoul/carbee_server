const Geocodio = require('geocodio');
const { GEOCODING } = require('../config');

const geoc = new Geocodio({ base_endpoint: GEOCODING.base_endpoint, api_key: GEOCODING.api_key });

module.exports = (params, path = 'geocode') =>
  new Promise((res, rej) => {
    geoc.get(path, params, (err, resp) => {
      if (err) res({});
      const respObject = JSON.parse(resp);
      res(respObject);
    });
  });

// { postal_code: 184041, country: 'Russia' }

/* function aaa(params, path = 'geocode') {
  new Promise((res, rej) => {
    geoc.get(path, params, (err, resp) => {
      if (err) rej(err);
      const respObject = JSON.parse(resp);
      res(respObject);
    });
  });
}

aaa({ postal_code: 60110 });
 */
