const jwt = require('jsonwebtoken');
const uuid = require('uuid/v4');
const { JWT } = require('../config');
// const bcryptjs = require('bcryptjs');

const tokenModule = {
  issuePareTokens(id) {
    return {
      token: jwt.sign({ id }, JWT.jwtSecret),
      refreshToken: uuid()
    };
  },
  issueSingleToken(payload = {}) {
    return {
      token: jwt.sign(payload, JWT.secret, { expiresIn: `${JWT.sessionActivity}s` }),
      expiresIn: JWT.sessionActivity
    };
  }
};

module.exports = tokenModule;
