const Nexmo = require('nexmo');
const { NEXMO: config } = require('../config');

const nexmo = new Nexmo(config);

module.exports = {
  requestSmsCode(number) {
    return new Promise((res, rej) => {
      nexmo.verify.request({ number, brand: config.brand }, (err, result) => {
        if (err) {
          rej(err);
        } else {
          res(result);
        }
      });
    });
  },
  verifyCode(requestId, code) {
    return new Promise((res, rej) => {
      nexmo.verify.check({ request_id: requestId, code }, (err, result) => {
        if (err) {
          rej(err);
        } else {
          res(result);
        }
      });
    });
  }
};
