const multer = require('koa-multer');
const { MULTER } = require('../config');
const { randId } = require('./helpers');

const storage = multer.diskStorage({
  destination: MULTER.STORAGE_DESTINATION,
  filename(ctx, file, cb) {
    cb(null, `${randId()}-${Date.now()}${path.extname(file.originalname)}`);
  }
});

module.exports = {
  async fields(fields) {
    try {
      return await multer({
        storage,
        limits: { fileSize: 8000000 }, // 8mb
        fileFilter(req, file, cb) {
          const filetypes = /png|jpe?g|gif/;
          const extname = filetypes.test(path.extname(file.originalname).toLowerCase());
          const mimetype = filetypes.test(file.mimetype);

          if (mimetype && extname) {
            return cb(null, true);
          }

          const error = new Error('Wrong file type');
          error.status = 422;
          return cb(error);
        }
      }).fields(fields)();
    } catch (e) {
      if (e.code && e.code === 'LIMIT_FILE_SIZE') {
        e.status = 413;
        e.message = 'File too large';
      }
      throw e;
    }
  }
};

// .fields([{ name: 'photos', maxCount: 10 }])
