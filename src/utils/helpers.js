exports.randId = (len = 36) =>
  `${Date.now().toString(len)}${Math.random()
    .toString(len)
    .substr(2, 5)}`;
