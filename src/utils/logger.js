const winston = require('winston');

let level;
switch (process.env.NODE_ENV) {
  case 'development':
    level = 'silly';
    break;
  case 'test':
    level = 'warn';
    break;
  case 'production':
  default:
    level = 'info';
}

const logger = winston.createLogger({
  level,
  transports: [
    new winston.transports.Console({
      format: winston.format.combine(winston.format.colorize(), winston.format.timestamp(), winston.format.simple())
    })
    /* new winston.transports.File({
      filename: logFile,
      maxsize: 10485760, // 10*1024*1024 bytes
      maxFiles: 5,
      tailable: true,
    }), */
  ],
  handleExceptions: false,
  exitOnError: false
});

// create stream for morgan
logger.stream = {
  write: message => logger.info(message)
};

module.exports = logger;
