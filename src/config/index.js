module.exports = {
  PORT: process.env.PORT || 8081,
  HOST: process.env.HOST || 'localhost',
  DB: {
    HOST: process.env.CB_DB_HOST || 'localhost',
    PORT: process.env.CB_DB_PORT || '27017',
    CB_DB_AUTH: process.env.CB_DB_AUTH || 'carbee',
    OPTIONS: {
      autoIndex: process.env.NODE_ENV !== 'production',
      autoReconnect: false,
      bufferCommands: false,
      bufferMaxEntries: 0,
      user: process.env.CB_DB_USER || '',
      pass: process.env.CB_DB_PASSWD || '',
      dbName: process.env.CB_DB_NAME || 'carbee',
      useNewUrlParser: true,
      keepAlive: true
    }
  },
  AUTH: {
    passwordSalt: 'CGoCAB$qNzh$h7W1T!#uBcIjp1%bMNN&SlbCHtqdQlkhqA#&U6kvILKgudG4xPDV'
  },
  JWT: {
    secret: process.env.JWT_SECRET || '9Zu$nPB9Z#XCt60637XvIn4l$gMz%@eFluoiC&dn6*MTYWEBhFQW2qB0qDxr%PR4',
    refreshTokenSecret:
      process.env.JWT_REFRESH_TOKEN_SECRET || '9Zu$nPB9Z#XCt60637XvIn4l$gMz%@eFluoiC&dn6*MTYWEBhFQW2qB0qDxr%PR4',
    tokenLife: process.env.JWT_TOKEN_LIFE || 900,
    refreshTokenLife: process.env.JWT_TOKEN_LIFE || 86400,
    sessionActivity: process.env.JWT_SESSION_ACTIVITY || 300,

    jwtSecret: process.env.JWT_SECRET || '9Zu$nPB9Z#XCt60637XvIn4l$gMz%@eFluoiC&dn6*MTYWEBhFQW2qB0qDxr%PR4'
  },
  pwd: process.cwd(),
  MULTER: {
    FILESIZE_LIMIT: process.env.MULTER_FILESIZE_LIMIT || 8000000, // 8mb
    STORAGE_DESTINATION: process.env.MULTER_DESTINATION || `${process.cwd()}/public`
  },
  PHOTO_HOSTING: {
    PHOTO_HOST: process.env.PHOTO_HOST || `http://${process.env.HOST || 'localhost'}:${process.env.PORT || 8081}`,
    PHOTO_PATH: process.env.PHOTO_PATH || '/public'
  },
  NEXMO: {
    apiKey: process.env.NEXMO_API_KEY || '04091a21',
    apiSecret: process.env.NEXMO_API_SECRET || 'qwOUAo8XhPfsf1rP',
    brand: process.env.NEXMO_BRAND || 'Carbee'
  },
  GEOCODING: {
    api_key: process.env.GEOCODIO_API_KEY || '',
    base_endpoint: process.env.GEOCODIO_BASE_ENDPOINT || 'https://api.geocod.io/v1.3'
  },
  TEMP_ACC_EXP: process.env.TEMP_ACC_EXP || 310,
  REDIS: {
    REDIS_HOST: process.env.REDIS_HOST || 'localhost',
    REDIS_PORT: process.env.REDIS_POST || 6379
  },
  IOREDIS: {
    HOST: process.env.IOREDIS_HOST || 'localhost',
    PORT: process.env.IOREDIS_PORT || 16379
  }
};
