const Koa = require('koa');

const app = new Koa();

// /* Init handlers */
require('./handlers')(app);

// /* Init routes */
require('./routes')(app);

// const server = require('http').createServer(app.callback());

const server = null;

// // require('./service/socket.io.service')(server);

module.exports = server || app;
