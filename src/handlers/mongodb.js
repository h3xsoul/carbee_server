/*
  Mongo connected in src/index
 */

const mongoose = require('mongoose');
// const beautifulUnique = require('mongoose-beautiful-unique-validation');

const logger = require('../utils/logger');
const { DB } = require('../config');

mongoose.Promise = Promise;

/* plugins */
// ValidationError for mongo id
// mongoose.plugin(beautifulUnique);

/* Environment settings */
if (process.env.NODE_ENV === 'development') mongoose.set('debug', true);
mongoose.set('useCreateIndex', true);
mongoose.set('useFindAndModify', false);

mongoose.connection.on('connecting', () => {
  logger.info('Mongoose connecting ... ');
});

mongoose.connection.on('connected', () => {
  logger.info('Mongoose connection open');
});

// When the connection is disconnected
mongoose.connection.on('disconnected', () => {
  logger.info('Mongoose connection disconnected');
});

module.exports = {
  connect: async () => {
    try {
      await mongoose.connect(
        `mongodb://${DB.HOST}:${DB.PORT}/${DB.CB_DB_AUTH}`,
        DB.OPTIONS
      );
    } catch (err) {
      logger.error(`Mongoose connection error: ${err}`);
      throw err;
    }
  },
  close: async () => {
    await mongoose.connection.close();
  },
  mongoose
};

/* module.exports = (app) => {
  // let tryes = 0;
  async function connect() {
    // logger.info('DB connection try', ++tryes);
    try {
      await mongoose.connect(`mongodb://${DB.HOST}:${DB.PORT}`, DB.OPTIONS);
    } catch (err) {
      logger.error(`Mongoose connection error: ${err}`);
      // if (process.env.NODE_ENV === 'development') mongoose.connection.close();
      // setTimeout(connect, settings.DB.options.reconnectInterval += 10)
    }
  }

  connect();

  app.on('close', () => {
    mongoose.connection.close();
  });
}; */
