const morgan = require('koa-morgan');

const logger = require('../utils/logger');

module.exports = app => app.use(morgan('combined', { stream: logger.stream }));
