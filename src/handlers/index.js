/* eslint-disable */
module.exports = app => {
  require('./logger')(app);
  require('./cors')(app);
  require('./errors')(app);
  require('./passport')(app);
};
/* eslint-enable */
