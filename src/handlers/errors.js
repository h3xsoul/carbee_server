const logger = require('../utils/logger');

module.exports = app => {
  app.use(async (ctx, next) => {
    try {
      await next();
    } catch (e) {
      if (e.status) {
        ctx.body = e;
        ctx.status = e.status;
        logger.log('info', e.message);
      } else {
        ctx.body = 'Error 500';
        ctx.status = 500;
        logger.log('error', e.message, { stack: e.stack });
      }
    }
  });

  app.on('error', (err, ctx) => {
    if (err.code === 'ENOENT') {
      logger.info(err.message);
    } else {
      logger.error(err.message);
    }
  });
};
