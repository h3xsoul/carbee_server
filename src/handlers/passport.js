const passport = require('koa-passport');
const FacebookTokenStrategy = require('passport-facebook-token');
const Customer = require('../models/customer');

module.exports = app => {
  passport.use(
    'facebook-token',
    new FacebookTokenStrategy(
      {
        clientID: 1865922610095540,
        clientSecret: 'e87aafcb4b5a9c790011a892c45704f8'
      },
      async (accessToken, refreshToken, profile, done) => {
        let error = null;
        let customer = await Customer.findOne({ 'auth.facebook.id': profile.id });

        if (!customer) {
          customer = await Customer.create({
            'auth.facebook': profile,
            'profile.name': profile.dislpayName
          });
        }

        if (!customer.phone) {
          error = new Error('Phone is requred');
          error.type = 'phone';
        }

        return done(error, customer);
      }
    )
  );

  app.use(passport.initialize());
};
