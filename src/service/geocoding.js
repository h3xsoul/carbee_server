const GeoData = require('../models/geodata');
const geocodio = require('../utils/geocodio');

module.exports = {
  async getLocation(zip) {
    const localGeoData = await GeoData.find({ 'address_components.zip': zip });
    if (localGeoData.length > 0) return localGeoData[0].location.toObject();

    const resp = await geocodio({ state: 'WI', postal_code: zip });
    if (!resp.results || resp.results.length <= 0) return null;

    const geocodioGeoData = resp.results.find(item => item.address_components.state === 'WI');
    if (!geocodioGeoData) return null;

    geocodioGeoData.location = { coordinates: [geocodioGeoData.location.lng, geocodioGeoData.location.lat] };

    const newGeoData = await GeoData.create(geocodioGeoData);

    return newGeoData.location.toObject();
  },
  async getHint(q) {
    if (!q) return [];
    const locationData = await geocodio(q);
    if (!locationData.results || locationData.results.length <= 0) return [];

    return locationData.results;
  },
  async checkEreaCondition(zip, state = 'WI') {
    const localGeoData = await GeoData.find({ 'address_components.zip': zip });
    if (localGeoData.length > 0) {
      const findResult = localGeoData.find(item => item.address_components.state === state);
      if (!findResult) return null;
      return findResult;
    }

    const respGeocodio = await geocodio({ state, postal_code: zip });
    if (!respGeocodio || !respGeocodio.results || respGeocodio.results.length <= 0) return null;

    respGeocodio.results = respGeocodio.results.map(item => {
      item.location = { coordinates: [item.location.lng, item.location.lat] };
      return item;
    });

    GeoData.insertMany(respGeocodio.results);

    const findResultResponse = respGeocodio.results.find(item => item.address_components.state === state);
    return findResultResponse;
  }
};
