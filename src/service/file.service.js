const fs = require('fs');

module.exports = {
  removeFile(fileName) {
    return new Promise(res => {
      fs.unlink(`${process.cwd()}/public/${fileName}`, err => {
        res(err ? { file: fileName, err: err.code } : fileName);
      });
    });
  }
};
