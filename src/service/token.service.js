const RedisJwt = require('redis-jwt');
// const uuidv4 = require('uuid/v4');
const { REDIS, JWT } = require('../config');

const redisJwt = new RedisJwt({
  host: REDIS.REDIS_HOST,
  port: REDIS.REDIS_PORT,
  maxretries: 10,
  secret: JWT.secret,
  multiple: true
});

module.exports = {
  async generate(user) {
    const token = await redisJwt.sign(`${user.id}`, {
      ttl: `${JWT.sessionActivity} seconds`,
      dataToken: user
    });

    return {
      token,
      ttl: JWT.sessionActivity
    };
  },

  async verify(...args) {
    return redisJwt.verify(...args);
  },

  async destroy(token) {
    const call = redisJwt.call();
    return call.destroy(token);
  }
};

// class TokenService {
//   constructor() {
//     this.JWT = new RedisJwt({
//       host: REDIS.REDIS_HOST,
//       port: REDIS.REDIS_PORT,
//       maxretries: 10,
//       secret: JWT.secret
//     });
//   }

//   async generate(user) {
//     const token = await this.JWT.sign(`${user.id}${uuidv1()}`, {
//       ttl: `${JWT.sessionActivity} seconds`,
//       dataToken: user
//     });

//     return {
//       token,
//       ttl: JWT.sessionActivity
//     };
//   }

//   async verify(token) {
//     return this.JWT.verify(token);
//   }

//   async destroy(token) {
//     const call = this.JWT.call();
//     return call.destroy(token);
//   }
// }

// module.exports = new TokenService();
