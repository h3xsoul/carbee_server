const io = require('socket.io')();
const ioRedis = require('socket.io-redis');
const { IOREDIS } = require('../config');
const logger = require('../utils/logger');
const RepairhsopSocket = require('../socket/repairhsop');
const CustomerSocket = require('../socket/customer');
const TokenService = require('../service/token.service');

async function Authentication(socket, next) {
  const { query } = socket.handshake;
  if (!query.authorization) return next(new Error('Authentication fail'));

  const decode = await TokenService.verify(query.authorization).catch(e => {});
  if (!decode) return next(new Error('Authentication fail'));
  socket.userdata = decode;
  socket.userdata.userRoomId = `${decode.dataToken.usertype}${decode.dataToken.id}`;
  await next();
}

module.exports = server => {
  io.adapter(ioRedis({ host: IOREDIS.HOST, port: IOREDIS.PORT }));

  io.attach(server, {
    path: '/socket-chat'
  });

  io.use(Authentication);

  io.on('connection', socket => {
    socket.join(socket.userdata.userRoomId);
    logger.debug(`Socket connected ${socket.id}`);

    socket.on('new message', data => {
      socket.to(`chat${data.chatId}`).emit('new message', data);
    });

    RepairhsopSocket(socket);
    CustomerSocket(socket);
  });
};

// const connectedUsers = {};

/* eslint class-methods-use-this: "off" */
// class SocketIO extends Socket {
//   constructor(...args) {
//     super(...args);

//     this.connectedUsers = {};

//     this.use(Authentication.bind(this));

//     this.on('connection', this.connect.bind(this));
//   }

//   connect(socket) {
//     this.addUser(socket);
//     logger.info(`Socket connection ${socket.id}`);

//     socket.on('new message', logger.info);

//     socket.on('disconnect', reason => {
//       logger.info(`Socket disconnected: ${reason}`);
//       this.removeUser(socket.userdata.userId);
//     });

//     socket.on('info test', data => {
//       console.log(data);
//       socket.to(`${socket.userdata.userId}`).emit('new notification', data);
//     });

//     socket.on('submit quote', data => {
//       if (`Customer${data.quote.customer}` in this.connectedUsers) {
//         const reciever = this.connectedUsers[`Customer${data.quote.customer}`].socketId;

//         socket.to(reciever).emit('new chat', data);
//       }
//     });

//     // RepairhsopSocket(socket);
//     // CustomerSocket(socket);
//   }

//   addUser(socket) {
//     this.connectedUsers[socket.userdata.userId] = {
//       socketId: socket.id,
//       user: socket.userdata.dataToken
//     };
//   }

//   removeUser(user) {
//     delete this.connectedUsers[user];
//   }
// }

// module.exports = SocketIO;
