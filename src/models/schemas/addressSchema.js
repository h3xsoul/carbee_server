const mongoose = require('mongoose');

const geolocationSchema = require('./geolocationSchema');

const { Schema } = mongoose;

module.exports = new Schema(
  {
    street: String,
    city: String,
    state: String,
    zip: String,
    coutry: String,
    location: geolocationSchema
  },
  { _id: false }
);
