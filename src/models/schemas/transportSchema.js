const mongoose = require('mongoose');

const { Schema } = mongoose;

module.exports = new Schema({
  make: String,
  model: String,
  year: String,
  type: String
});
