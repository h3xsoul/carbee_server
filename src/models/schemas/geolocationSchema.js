const mongoose = require('mongoose');

module.exports = new mongoose.Schema(
  {
    type: {
      type: String,
      default: 'Point'
    },
    coordinates: {
      type: [Number] // lng: Number, // lat: Number
    } // order from geocodio lat: 43.037213, lng: -87.933529
  },
  { _id: false }
);
