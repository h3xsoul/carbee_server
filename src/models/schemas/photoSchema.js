const mongoose = require('mongoose');

module.exports = new mongoose.Schema(
  {
    url: String,
    path: {
      type: String,
      select: false
    },
    linkpath: String,
    filename: String,
    description: String
  },
  { _id: false }
);
