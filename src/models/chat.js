const mongoose = require('mongoose');

const { Schema } = mongoose;

const schatSchema = new Schema(
  {
    quote: {
      type: Schema.Types.ObjectId,
      ref: 'Quote'
    },
    customer: {
      type: Schema.Types.ObjectId,
      ref: 'Customer'
    },
    repairshop: {
      type: Schema.Types.ObjectId,
      ref: 'Repairshop'
    },
    price: String,
    hours: String,
    minutes: String,
    isActive: {
      type: Boolean,
      default: true
    }
  },
  { timestamps: true }
);

module.exports = mongoose.model('Chat', schatSchema);
