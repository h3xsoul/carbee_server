const mongoose = require('mongoose');

const { Schema } = mongoose;

const transportSchema = require('./schemas/transportSchema');
const addressSchema = require('./schemas/addressSchema');

const customerSchema = new Schema(
  {
    auth: {
      type: {
        facebook: {
          id: String,
          emails: [
            {
              value: String
            }
          ],
          dislpayName: String,
          photos: [
            {
              value: String
            }
          ]
        },
        local: {
          email: String,
          emailVerified: Boolean,
          phone: String,
          phoneVerified: Boolean,
          password: {
            type: String,
            select: false
          }
        }
      },
      select: false
    },
    profile: {
      fName: String,
      lName: String,
      address: addressSchema
    },
    phone: String,
    garage: [transportSchema],
    active: {
      type: Boolean,
      default: false
    }
  },
  { timestamps: true }
);

module.exports = mongoose.model('Customer', customerSchema);
