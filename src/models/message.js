const mongoose = require('mongoose');

const { Schema } = mongoose;

const messageSchema = new Schema(
  {
    chat: {
      type: Schema.Types.ObjectId,
      ref: 'Chat'
    },
    body: {
      type: String,
      trim: true,
      required: true
    },
    author: {
      type: Schema.Types.ObjectId,
      refPath: 'type',
      index: true
    },
    type: {
      type: String,
      enum: ['Customer', 'Repairshop'],
      require: true
    }
  },
  { timestamps: true }
);

module.exports = mongoose.model('Message', messageSchema);
