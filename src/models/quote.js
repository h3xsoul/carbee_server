const mongoose = require('mongoose');

const { Schema } = mongoose;

const geoloactionSchema = require('./schemas/geolocationSchema');

const quoteSchema = new Schema(
  {
    transport: Object,
    customer: { type: Schema.Types.ObjectId, ref: 'Customer' },
    description: String,
    // chats: [
    //   {
    //     repairshop: { type: Schema.Types.ObjectId, ref: 'Repairshop' },
    //     active: { type: Boolean, default: true },
    //     created_at: { type: Date, default: Date.now }
    //   }
    // ],
    address: {
      zip: String
    },
    location: geoloactionSchema,
    photos: [
      {
        name: String,
        path: String,
        _id: false
      }
    ],
    direct: {
      type: Boolean,
      default: false
    }
  },
  { timestamps: true }
);

quoteSchema.index({ location: '2dsphere' });

module.exports = mongoose.model('Quote', quoteSchema);

/* const photoSchema = require('./schemas/photoSchema');

const geoLocationSchema = require('./schemas/geolocationSchema'); */

/* const quoteModel = new mongoose.Schema(
  {
    customer: {
      type: mongoose.Schema.Types.ObjectId,
      ref: 'Customer',
      index: true,
      required: false
    },
    title: String,
    description: String,
    zip: String,
    transport: {
      type: mongoose.Schema.Types.ObjectId,
      ref: 'Transport'
    },
    photos: [
      {
        type: photoSchema
      }
    ],
    location: geoLocationSchema,
    responses: [
      {
        type: new mongoose.Schema(
          {
            autoservice_id: {
              type: mongoose.Schema.Types.ObjectId,
              ref: 'autoservice'
            },
            description: String,
            price: {
              amount: String,
              hours: String
            }
          },
          { timestamps: true }
        )
      }
    ]
  },
  { timestamps: true }
);

quoteModel.index({ location: '2dsphere' });

quoteModel.statics.isValidId = mongoose.Types.ObjectId.isValid;
quoteModel.statics.publicFields = ['id', 'title', 'description', 'phone', 'zip', 'car', 'photos']; */

// module.exports = mongoose.model('quote', quoteModel);
