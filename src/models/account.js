const mongoose = require('mongoose');

const { Schema } = mongoose;

const accountModel = new Schema(
  {
    auth: {
      local: {
        email: String,
        email_verified: {
          type: Boolean,
          default: false
        },
        username: String,
        phone: {
          type: String,
          index: true
        },
        phone_verified: {
          type: Boolean,
          default: false
        },
        code_request_id: String,
        password: {
          type: String,
          minlength: 6
        },
        salt: {
          type: String
        }
      }
    },
    type: {
      type: String,
      enum: ['Customer', 'Repairshop'],
      require: true
    },
    entity: {
      type: Schema.Types.ObjectId,
      refPath: 'type',
      index: true
    },
    deleted: {
      type: Boolean,
      enum: [true, false],
      default: false
    },
    active: {
      type: Boolean,
      default: false
    },
    tokens: [
      {
        access: {
          type: String,
          required: true
        },
        token: {
          type: String,
          required: true
        }
      }
    ]
  },
  { timestamps: true }
);

accountModel.statics.accType = {
  customer: 'Customer',
  service: 'Service'
};

module.exports = mongoose.model('Account', accountModel);
