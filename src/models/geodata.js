const mongoose = require('mongoose');

const { Schema } = mongoose;

const geolocationSchema = require('./schemas/geolocationSchema');

const geoDataModel = new Schema({
  address_components: {
    city: String, //Milwaukee,
    county: String, //Milwaukee County,
    state: String, //WI,
    zip: {
      type: String,
      index: true
    }, //53233,
    country: String //US
  },
  formatted_address: String, //Milwaukee, WI 53233,
  location: geolocationSchema,
  accuracy: String, //1,
  accuracy_type: String, //place,
  source: String //TIGER/Line® dataset from the US Census Bureau
});

geoDataModel.index({ location: '2dsphere' });

module.exports = mongoose.model('GeoData', geoDataModel);
