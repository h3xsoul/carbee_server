const mongoose = require('mongoose');

const { Schema } = mongoose;

const messageSchema = new Schema({
  from: { type: Schema.Types.ObjectId, refPath: 'type' },
  type: { type: String, enum: ['Customer', 'Autoservice'] },
  text: String,
  chat_id: Schema.Types.ObjectId
});

module.exports = mongoose.model('Message', messageSchema);
