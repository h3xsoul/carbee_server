const mongoose = require('mongoose');

const { Schema } = mongoose;

const addressSchema = require('./schemas/addressSchema');
const geolocationSchema = require('./schemas/geolocationSchema');

const repairshopModel = new Schema(
  {
    auth: {
      type: {
        local: {
          email: String,
          emailVerified: {
            type: Boolean,
            default: false
          },
          phone: String,
          phoneVerified: {
            type: Boolean,
            default: false
          },
          password: {
            type: String,
            require: true,
            select: false
          }
        }
      },
      select: false
    },
    profile: {
      businessName: String,
      phones: [String],
      emails: [String],
      address: addressSchema,
      location: geolocationSchema,
      about: String,
      photos: [String],
      hero: String,
      avatar: String,
      options: [String],
      social_links: [String]
    }
  },
  { timestamps: true }
);

module.exports = mongoose.model('Repairshop', repairshopModel);
