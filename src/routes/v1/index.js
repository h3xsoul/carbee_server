const Router = require('koa-router');

const apiV1 = new Router({
  prefix: '/v1'
});

// apiV1.use(require('./geo'));
// apiV1.use(require('./quotes'));
// apiV1.use(require('./auth'));
// apiV1.use(require('./account'));

apiV1.use(require('./customer'));
apiV1.use(require('./repairshop'));
// apiV1.use(require('./garage'));

module.exports = apiV1.routes();
