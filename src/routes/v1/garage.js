const Router = require('koa-router');
const koaJwt = require('koa-jwt');
const koaBody = require('koa-body');

const { JWT } = require('../../config');

const Account = require('../../models/account');

const checkAccount = async (ctx, next) => {
  const { user } = ctx.state;
  const account = await Account.findById(user.id);
  if (!account || account.type === 'Repairshop') return ctx.throw(404, 'Account not found');

  await account.populate('entity').execPopulate();
  ctx.state.customer = account.entity;
  await next();
};

const garage = new Router({
  prefix: '/garage'
});

garage.use(
  koaJwt({
    secret: JWT.jwtSecret
  })
);

garage.get('/', checkAccount, koaBody(), async ctx => {
  const { account } = ctx.state;
  await account.populate('entity').execPopulate();
  ctx.body = { garage: account.entity.garage };
});

garage.post('/', checkAccount, koaBody(), async ctx => {
  const { customer } = ctx.state;
  const { body } = ctx.request;
  customer.garage.push(body);
  await customer.save();
  // await customer.updateOne({ garage: { $push: { ...body } } });
  ctx.body = { garage: customer.garage };
  ctx.status = 200;
});

garage.delete('/:carId', checkAccount, koaBody(), async ctx => {
  const { customer } = ctx.state;
  const { carId } = ctx.params;

  customer.garage.pull(carId);
  await customer.save();

  ctx.body = { garage: customer.garage };
});

module.exports = garage.routes();
