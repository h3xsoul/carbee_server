const Router = require('koa-router');
const koaBody = require('koa-body');
const GeocodingService = require('../../../service/geocoding');

// const photoUploader = require('../../../utils/photoUploader');

/* Services */
const UploadService = require('../../../service/upload.service');
const FileService = require('../../../service/file.service');

/* Models */
const Quote = require('../../../models/quote');
const Customer = require('../../../models/customer');

/* Middlewares */
const Authenticate = require('../../../middleware/auth.middleware');

const quoteRouter = new Router({
  prefix: '/quotes'
});

quoteRouter.use(Authenticate);

/* quoteRouter.post('/', koaBody(), async ctx => {
  const { body } = ctx.request;
  const location = await GeocodingService.getLocation(body.address.zip);
  if (!location) return ctx.throw(404, 'Your location is not in our service yet or zip code was not found');

  // await photoUploader.fields([{ name: 'photos', maxCount: 10 }]);
  // const { photos } = ctx.req.files;
  const { customer } = ctx.state;

  const quote = await Quote.create({
    car: body.car_id,
    customer: customer.id,
    description: body.description,
    address: body.address,
    location
    // photos: [{}]
  });

  ctx.body = quote.toObject();
}); */

quoteRouter.get('/', async ctx => {
  const { user } = ctx.state;

  const quotes = await Quote.find({ customer: user.id }, '-customer')
    .lean()
    .exec();

  ctx.body = quotes;
});

quoteRouter.delete('/:quoteId', async ctx => {
  const { user } = ctx.state;
  const { quoteId } = ctx.params;

  const quote = await Quote.findOneAndRemove({ _id: quoteId, customer: user.id });

  const removeFiles = quote.photos.map(photo => FileService.removeFile(photo.name));

  const removedFiles = await Promise.all(removeFiles);
  console.log(removedFiles);

  ctx.body = {
    quote: quote.toObject()
  };
});

quoteRouter.post('/', koaBody(), async ctx => {
  const { user } = ctx.state;
  const { transport = null, description = '', zip = '' } = ctx.request.body;

  const location = await GeocodingService.getLocation(zip);
  if (!location) return ctx.throw(404, 'Your location is not in our service yet or zip code was not found');

  const customer = await Customer.findOne({ _id: user.id, 'garage._id': transport._id });

  if (!customer) return ctx.throw(404, 'Customer or transport not found');

  const quote = await Quote.create({
    transport,
    customer: customer.id,
    description,
    location,
    address: {
      zip
    }
  });

  ctx.body = { id: quote.id };
});

quoteRouter.post('/images', async ctx => {
  await UploadService.fields([{ name: 'images', maxCount: 6 }, { name: 'id' }])(ctx);
  const { images } = ctx.req.files;
  const { id } = ctx.req.body;

  const photos = images.map(file => ({
    name: file.filename,
    path: '/public'
  }));

  const quote = await Quote.findByIdAndUpdate(id, { $push: { photos } }, { new: true });

  ctx.body = quote;
});

module.exports = quoteRouter.routes();
