const Router = require('koa-router');
const koaBody = require('koa-body');
const passport = require('koa-passport');

const Nexmo = require('../../../utils/nexmo');

const GeocodingService = require('../../../service/geocoding');
const CryptService = require('../../../utils/crypt');
const TokenService = require('../../../service/token.service');

const Authentication = require('../../../middleware/auth.middleware');

const Customer = require('../../../models/customer');

const authRouter = new Router({
  prefix: '/auth'
});

authRouter.use(koaBody());

authRouter.post('/local/sms_code', async ctx => {
  // ctx.body = ctx.request.url;

  const { phone, zipcode } = ctx.request.body;

  const fixedPhone = phone.length === 10 ? `1${phone}` : phone;

  let account = null;
  account = await Customer.findOne({ 'auth.local.phone': fixedPhone, 'auth.local.phoneVerified': true });
  if (account) return ctx.throw(409, 'Phone number already exist', { exist: true, field: 'phone', phone: fixedPhone });

  if (!(await GeocodingService.checkEreaCondition(zipcode)))
    return ctx.throw(409, 'Service not awailable at this area', { field: 'zipcode', zipcode });

  const nexmoResp = await Nexmo.requestSmsCode(fixedPhone);

  if (!nexmoResp.request_id) return ctx.throw(502, 'Something wrong with provider. Please, try later');

  ctx.body = {
    requestId: nexmoResp.request_id
  };
});

authRouter.post('/local/register', async ctx => {
  // ctx.body = ctx.request.url;

  const { fName, lName, phone, password, smsCode, zipcode, requestId } = ctx.request.body;

  const fixedPhone = phone.length === 10 ? `1${phone}` : phone;

  let account = null;
  account = await Customer.findOne({ 'auth.local.phone': fixedPhone, 'auth.local.phoneVerified': true });
  if (account) return ctx.throw(409, 'Phone number already exist', { exist: true, field: 'phone', phone: fixedPhone });

  const geoData = await GeocodingService.checkEreaCondition(zipcode);
  if (!geoData) return ctx.throw(409, 'Service not awailable at this area', { field: 'zipcode', zipcode });

  const nexmoResp = await Nexmo.verifyCode(requestId, smsCode);
  if (Number(nexmoResp.status) !== 0) ctx.throw(409, 'Code does not match', nexmoResp);

  const customer = await Customer.create({
    'auth.local.phone': phone,
    'auth.local.phoneVerified': true,
    'profile.address': {
      ...geoData.address_components,
      location: geoData.location
    },
    profile: {
      lName,
      fName
    },
    phone
  });

  const hashedPassword = await CryptService.generateHash(password);

  await customer.updateOne({ 'auth.local.password': hashedPassword });

  const issuedToken = await TokenService.generate({ id: customer.id, usertype: 'Customer' });

  ctx.body = issuedToken;
});

authRouter.post('/local/login', async ctx => {
  // ctx.body = ctx.request.url;

  const { phone, password } = ctx.request.body;

  const fixedPhone = phone.length === 10 ? `1${phone}` : phone;

  const customer = await Customer.findOne({ 'auth.local.phone': fixedPhone }).select('auth.local.password');

  if (!customer) return ctx.throw(422, 'Wrong login or password');

  const isEqual = await CryptService.compare(password, customer.auth.local.password);
  if (!isEqual) return ctx.throw(422, 'Wrong login or password');

  const issuedToken = await TokenService.generate({ id: customer.id, usertype: 'Customer' });

  ctx.body = issuedToken;
});

authRouter.get('/local/logout', Authentication, async ctx => {
  const { user } = ctx.state;

  await TokenService.destroy(user.rjwt);

  ctx.status = 201;
});

authRouter.post('/facebook', async ctx => {
  passport.authenticate('facebook-token', { session: false }, async (err, user, info, status) => {
    const response = {};
    if (err.type === 'phone') {
      response.error = err.type;
    }
    const token = await TokenService.generate({ id: user.id, usertype: 'Customer' });
    response.token = token;
    ctx.body = response;
  })(ctx);
});

authRouter.post('/facebook/sms_code', Authentication, async ctx => {
  const { phone } = ctx.request.body;
  const { user } = ctx.state;
});

module.exports = authRouter.routes();
