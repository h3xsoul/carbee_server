const Router = require('koa-router');
/* const photoUploader = require('../../../utils/photoUploader');
const Geocodio = require('../../../utils/geocoding');

const Quote = require('../../../models/quote');
const Account = require('../../../models/account');
const GeoData = require('../../../models/geodata'); */

const TokenService = require('../../../service/token.service');

const Authentication = require('../../../middleware/auth.middleware');

const Customer = require('../../../models/customer');

const customerRouter = new Router({
  prefix: '/customer'
});

customerRouter.use(require('./auth'));

// customerRouter.use(
//   koaJwt({
//     secret: JWT.jwtSecret
//   })
// );

customerRouter.use(Authentication);

customerRouter.use(require('./garage'));
customerRouter.use(require('./quote'));
customerRouter.use(require('./chats'));

customerRouter.get('/', async ctx => {
  const { user } = ctx.state;

  const customer = await Customer.findById(user.id);
  if (!customer) {
    await TokenService.destroy(user.rjwt);
    return ctx.throw('404', 'Customer not found');
  }

  ctx.body = customer.toObject();
});
/* custmrRouter.post('/quote', koaBody(), async ctx => {
  await photoUploader.fields([{ name: 'photos', maxCount: 10 }]);

  const { id: accountId } = ctx.state;

  // const { photos: files } = ctx.req.files;

  const { car_id, decription, zip } = ctx.request.body;

  const account = await Account.findById(accountId)
    .populate('entity')
    .exec();

  if (!account) ctx.throw(404);

  let geoData = await GeoData.findOne({ 'address_components.zip': zip });

  if (!geoData) {
    let geocodio;
    try {
      geocodio = await Geocodio({ postal_code: zip, country: 'USA' });
    } catch (e) {
      ctx.throw(e);
    }

    if (geocodio.results.length <= 0) {
      ctx.throw(404, 'Your location is not in our service yet or zip code was not found');
    }

    const newgeoData = geocodio.results[0];

    newgeoData.location = { coordinates: [newgeoData.location.lng, newgeoData.location.lat] };

    geoData = await GeoData.create(newgeoData);
  }

  const newQuote = await Quote.create({
    customer: account.entity.id,
    decription,
    zip,
    transport: car_id,
    location: geoData.location
  });

  await newQuote
    .populate('customer')
    .populate('transport')
    .execPopulate();

  ctx.body = newQuote.toObject();
}); */

/* custmrRouter.get('/quote', async ctx => {
  const newTransport = await Transport.create({ make: 'Toyota', model: 'Corolla', year: '2007' });
  const newCustomer = await Customer.create({ name: 'Artem', transports: [{ transport_id: newTransport.id }] });

  const newAccount = await Account.create({
    type: 'Customer',
    entity: newCustomer.id
  });

  ctx.body = newAccount.toObject();
}); */

module.exports = customerRouter.routes();
