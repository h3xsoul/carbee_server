const koaBody = require('koa-body');
const Router = require('koa-router');

const Customer = require('../../../models/customer');

const garageRouter = new Router({
  prefix: '/garage'
});

garageRouter.get('/', async ctx => {
  const { user } = ctx.state;
  const garageData = await Customer.findById(user.id, { _id: 0, garage: 1 }, { lean: true });
  if (!garageData) return ctx.throw(404, 'Nothing is here');
  ctx.body = garageData;
});

garageRouter.post('/', koaBody(), async ctx => {
  const { user } = ctx.state;
  const { make = '', model = '', year = '' } = ctx.request.body;
  const customer = await Customer.findByIdAndUpdate(
    user.id,
    { $push: { garage: { make, model, year } } },
    { new: true }
  );
  ctx.body = customer;
});

garageRouter.delete('/:transportId', koaBody(), async ctx => {
  const { user } = ctx.state;
  const { transportId } = ctx.params;

  const customer = await Customer.findByIdAndUpdate(
    user.id,
    { $pull: { garage: { _id: transportId } } },
    { new: true }
  );

  ctx.body = {
    garage: customer.garage.toObject()
  };
});

module.exports = garageRouter.routes();
