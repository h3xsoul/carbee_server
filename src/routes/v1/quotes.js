const fs = require('fs');
const path = require('path');
const pick = require('lodash/pick');
const Router = require('koa-router');
const koaBody = require('koa-body');
const multer = require('koa-multer');
const logger = require('../../utils/logger');
const { pwd, PHOTO_HOSTING } = require('../../config');
const { randId } = require('../../utils/helpers');

const Quote = require('../../models/quote');

const storage = multer.diskStorage({
  destination: `${pwd}/public`,
  filename(ctx, file, cb) {
    cb(null, `${randId()}-${Date.now()}${path.extname(file.originalname)}`);
  }
});

const uploader = multer({
  storage,
  limits: { fileSize: 8000000 }, // 8mb
  fileFilter(req, file, cb) {
    const filetypes = /png|jpe?g|gif/;
    const extname = filetypes.test(path.extname(file.originalname).toLowerCase());
    const mimetype = filetypes.test(file.mimetype);

    if (mimetype && extname) {
      return cb(null, true);
    }

    const error = new Error('Wrong file type');
    error.status = 422;
    return cb(error);
  }
}).fields([{ name: 'photos', maxCount: 10 }]);

const quotes = new Router({
  prefix: '/quotes'
});

quotes.param('id', async (id, ctx, next) => {
  if (!Quote.isValidId(id)) {
    ctx.throw(404);
  }

  await next();
});

quotes.get('/', koaBody(), async ctx => {
  const { lastSeen, results = 6 } = ctx.query;

  let quoteList;

  if (!lastSeen) {
    quoteList = await Quote.find({})
      .sort({ _id: -1 })
      .limit(results)
      .exec();
  } else {
    quoteList = await Quote.find({ _id: { $lt: lastSeen } })
      .sort({ _id: -1 })
      .limit(results)
      .exec();
  }

  ctx.body = [];

  if (quoteList.length > 0) {
    ctx.body = quoteList.map(document => ({
      id: document.id,
      ...pick(document.toObject(), Quote.publicFields)
    }));
  }
});

quotes.get('/:id', async ctx => {
  const quote = await Quote.findById(ctx.params.id);

  if (!quote) ctx.throw(404);

  ctx.body = {
    id: quote.id,
    ...pick(quote.toObject(), Quote.publicFields)
  };
});

quotes.delete('/:id', async ctx => {
  const { id } = ctx.params;
  const quote = await Quote.findById(id)
    .select('+photos.path')
    .exec();

  quote.photos.forEach(photo => {
    fs.unlink(photo.path, err => {
      if (err) {
        logger.log('error', err.message, { url: photo.url });
      }
    });
  });

  await quote.remove();

  ctx.status = 204;
});

quotes.post('/', async ctx => {
  try {
    await uploader(ctx);
  } catch (e) {
    if (e.code && e.code === 'LIMIT_FILE_SIZE') {
      ctx.throw(413, 'File too large');
    } else {
      ctx.throw(e);
    }
  }

  const { body } = ctx.req;
  const { photos: files } = ctx.req.files;

  const car = {
    make: body.make,
    model: body.model,
    year: body.year
  };

  const quoteEntry = { ...body, car };

  if (files && files.length > 0) {
    const links = files.map(item => ({
      url: `${PHOTO_HOSTING.PHOTO_HOST}${PHOTO_HOSTING.PHOTO_PATH}/${item.filename}`,
      path: item.path,
      linkpath: `${PHOTO_HOSTING.PHOTO_PATH}/${item.filename}`,
      filename: `${item.filename}`
    }));

    quoteEntry.photos = links;
  }

  const quote = new Quote(quoteEntry);

  await quote.save();

  ctx.body = {
    id: quote.id,
    ...pick(quote.toObject(), Quote.publicFields)
  };
});

module.exports = quotes.routes();
