const Router = require('koa-router');
const koaBody = require('koa-body');
const tokenService = require('../../utils/token');
const TokenService = require('../../service/token.service');
const GeocodingService = require('../../service/geocoding');
const cryptService = require('../../utils/crypt');
const Nexmo = require('../../utils/nexmo');

const Authentication = require('../../middleware/auth.middleware');

const auth = new Router({
  prefix: '/auth'
});

const Account = require('../../models/account');
const Customer = require('../../models/customer');
const Repairshop = require('../../models/repairshop');

const Entitys = {
  Customer,
  Repairshop
};

auth.post('/local/sms_code', koaBody(), async ctx => {
  const { email = '', phone = '', agreed = false, zip = '' } = ctx.request.body;

  // TODO check form fileds!

  /* Check if user agreed with Terms */
  if (!agreed) return ctx.throw(409, 'Not agreed');

  if (!(await GeocodingService.checkEreaCondition(zip)))
    return ctx.throw(404, 'service not awailable on this area', { zip });

  // TODO make check for country code 1
  const fixedPhone = `1${phone}`;

  let account = null;
  account = await Account.findOne({ 'auth.local.phone': fixedPhone });
  if (account && account.auth.local.phone_verified)
    return ctx.throw(409, 'phone already exist', { exist: true, field: 'phone', phone: fixedPhone });
  const accountEmail = await Account.findOne({ 'auth.local.email': email });
  if (accountEmail) return ctx.throw(403, 'email already exist', { exist: true, field: 'email', email });

  // const nexmoResp = await Nexmo.verifyRequest(phone);
  const nexmoResp = { request_id: 5555 };

  if (!nexmoResp.request_id) ctx.throw(502, 'Something wrong with provider');

  if (account) {
    await account.updateOne({ 'auth.local.code_request_id': nexmoResp.request_id });
  } else {
    account = await Account.create({
      'auth.local': {
        phone: fixedPhone,
        email,
        code_request_id: nexmoResp.request_id
      }
    });
  }

  ctx.body = {
    request_id: account.auth.local.code_request_id
  };
});

auth.post('/local/check', koaBody(), async ctx => {
  const { accType, email, phone } = ctx.request.body;
  let account = null;

  switch (accType) {
    case 'Customer':
    case 'Repairshop':
      account = await Account.findOne({ 'auth.local.phone': phone });
      if (account) return ctx.throw(409, 'phone already exist', { exist: true, field: 'phone', phone });

      account = await Account.findOne({ 'auth.local.email': email });
      if (account) return ctx.throw(403, 'email already exist', { exist: true, field: 'email', email });
      break;
    default:
      return ctx.throw(404, 'account type not found', { field: { accType } });
  }

  ctx.body = { exist: false };
});

auth.post('/local/register', koaBody(), async ctx => {
  const {
    accType = '',
    email = '',
    phone = '',
    agreed = false,
    request_id = null,
    code = '',
    password = '',
    name = ''
  } = ctx.request.body;

  // TODO check form fileds!

  /* Check if user agreed with Terms */
  if (!agreed) return ctx.throw(409, 'Not agreed');

  // TODO make check for country code 1
  const fixedPhone = `1${phone}`;

  let account = null;
  account = await Account.findOne({
    'auth.local.phone': fixedPhone,
    'auth.local.email': email,
    'auth.local.code_request_id': request_id
  });
  if (!account) return ctx.throw(404, 'request not found');
  // if (account && account.auth.local.phone_verified) return ctx.throw(409, 'Phone number already exist');

  // const nexmoResp = await Nexmo.verifyCheck(request_id, code);
  const nexmoResp = { status: 0 };

  if (Number(nexmoResp.status) !== 0) ctx.throw(409, 'Code does not match');

  const entity = await Entitys[accType].create({ name });

  const hashedPassword = await cryptService.generateHash(password);

  // const issuedToken = tokenService.issueSingleToken({ id: account.id, accType: account.type });
  const issuedToken = await TokenService.generate({ id: account.id, accType });

  await account.updateOne({
    type: accType,
    entity: entity.id,
    'auth.local.password': hashedPassword,
    'auth.local.phone_verified': true,
    'auth.local.code_request_id': null
  });

  ctx.body = issuedToken;
});

auth.post('/local/login', koaBody(), async ctx => {
  const user = ctx.request.body;

  if (!user.phone || !user.password) return ctx.throw(409, 'Missing credentials');

  // TODO change check for country code 1
  const account = await Account.findOne({ 'auth.local.phone': `1${user.phone}` });

  if (!account) return ctx.throw(422, 'Wrong login or password');

  const isEqual = await cryptService.compare(user.password, account.auth.local.password);
  if (!isEqual) return ctx.throw(422, 'Wrong login or password');

  // const issuedToken = tokenService.issueSingleToken({ id: account.id, accType: account.type });
  const issuedToken = await TokenService.generate({ id: account.id, accType: account.type });

  ctx.body = issuedToken;
});

auth.get('/local/logout', Authentication, async ctx => {
  const { user } = ctx.state;
  await TokenService.destroy(user.rjwt);
  ctx.status = 203;
});

auth.post('/local/createUser', koaBody(), async ctx => {
  const { body } = ctx.request;

  const entity = await Entitys[body.accType].create({ name: body.name });

  const account = await Account.create({
    'auth.local.phone': body.phone,
    'auth.local.password': await cryptService.generateHash(body.password),
    type: body.accType,
    entity: entity.id
  });

  const issuedToken = tokenService.issueSingleToken({ id: account.id, accType: account.type });

  ctx.body = issuedToken;
});

auth.post('/local/getacc', koaBody(), async ctx => {
  const { id } = ctx.request.body;

  const account = await Account.findById(id)
    .populate('entity')
    .exec();

  ctx.body = account.toObject();
});

module.exports = auth.routes();
