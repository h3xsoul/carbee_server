const Router = require('koa-router');
const koaBody = require('koa-body');
const geocoding = require('../../service/geocoding');

const geo = new Router({
  prefix: '/geo'
});

geo.get('/', koaBody(), async ctx => {
  const { query } = ctx.request;
  const results = await geocoding.getHint(query);
  ctx.body = results;
});

module.exports = geo.routes();
