const Router = require('koa-router');
const _ = require('lodash');
const koaJwt = require('koa-jwt');
const { JWT } = require('../../config');

const Authentication = require('../../middleware/auth.middleware');

const acc = new Router({
  prefix: '/account'
});

const Account = require('../../models/account');

/* JWT check middleware: ctx.state */

acc.use(Authentication);

/* acc.use(
  koaJwt({
    secret: JWT.jwtSecret
  })
); */

acc.get('/', async ctx => {
  const { user } = ctx.state;

  const account = await Account.findOne({ _id: user.id, type: user.accType });
  if (!account) return ctx.throw(404);

  await account.populate('entity').execPopulate();

  const bodyPick = {
    Customer: ['name'],
    Repairshop: ['name', 'avatar']
  };

  const body = _.pick(account.entity, bodyPick[user.accType]);
  body.type = account.type;

  ctx.body = body;
});

module.exports = acc.routes();
