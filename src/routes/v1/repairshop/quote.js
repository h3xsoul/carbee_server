const { ObjectId } = require('mongoose').Types;
const Router = require('koa-router');
const koaBody = require('koa-body');

const GeocodingService = require('../../../service/geocoding');

const Quote = require('../../../models/quote');
const Customer = require('../../../models/customer');
const Repairshop = require('../../../models/repairshop');
const Chat = require('../../../models/chat');

const quoteRouter = new Router({
  prefix: '/quotes'
});

quoteRouter.use(koaBody());

quoteRouter.get('/', async ctx => {
  const { user } = ctx.state;
  const { limit = 10, radius = 5, cursor = '', zip = '' } = ctx.query;

  const location = await GeocodingService.getLocation(zip);

  if (!location) return (ctx.body = []);

  const aggregator = [
    {
      $geoNear: {
        near: location, // { type: 'Point', coordinates: [-87.933529, 43.037213] },
        spherical: true,
        key: 'location',
        distanceField: 'distance.inMeters',
        maxDistance: radius * 1609.344
      }
    }
  ];

  if (cursor)
    aggregator.push({
      $match: {
        _id: { $gt: ObjectId(cursor) }
      }
    });

  // aggregator.push({
  //   $limit: limit
  // });

  aggregator.push({
    $addFields: {
      distance: {
        inMeters: '$distance.inMeters',
        inMiles: { $divide: ['$distance.inMeters', 1609.344] }
      }
    }
  });

  const quoteList = await Quote.aggregate(aggregator);

  ctx.body = quoteList;
});

quoteRouter.post('/', async ctx => {
  const { user } = ctx.state;
  const { quote, message, price, hours, minutes } = ctx.request.body;

  const qSearchResult = await Quote.findOne({ _id: quote._id });

  const chat = await Chat.create({
    quote: qSearchResult.id,
    customer: qSearchResult.customer,
    repairshop: user.id
  });

  ctx.body = chat.toObject();
});

module.exports = quoteRouter.routes();
