const koaBody = require('koa-body');
const Router = require('koa-router');

const testHook = new Router({
  prefix: '/testhook'
});

testHook.post('/', koaBody(), async ctx => {
  const { user } = ctx.state;
  const { body } = ctx.request;

  ctx.body = {
    ok: true
  };
});

module.exports = testHook.routes();
