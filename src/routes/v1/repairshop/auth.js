const Router = require('koa-router');
const koaBody = require('koa-body');

const GeocodingService = require('../../../service/geocoding');
const CryptService = require('../../../utils/crypt');
const TokenService = require('../../../service/token.service');

const Authentication = require('../../../middleware/auth.middleware');

const Repairshop = require('../../../models/repairshop');

const authRouter = new Router({
  prefix: '/auth'
});

authRouter.use(koaBody());

authRouter.post('/local/sms_code', async ctx => {
  // ctx.body = ctx.request.url;

  const { email = '', phone = '', agreed = false, zip = '' } = ctx.request.body;

  // if (!agreed) return ctx.throw(409, 'Not agreed');

  if (!(await GeocodingService.checkEreaCondition(zip)))
    return ctx.throw(404, 'Service not awailable on this area', { zip });

  const fixedPhone = `1${phone}`;

  /* Check if account exist */
  let account = null;
  account = await Repairshop.findOne({ 'auth.local.phone': fixedPhone, 'auth.local.phone_verified': true });

  if (account) return ctx.throw(409, 'Phone number already exist', { exist: true, field: 'phone', phone: fixedPhone });

  account = await Repairshop.findOne({ 'auth.local.email': email, 'auth.local.email.email_verified': true });
  if (account) return ctx.throw(409, 'Email already exist', { exist: true, field: 'email', email });

  const nexmoResp = { request_id: 5555 };

  if (!nexmoResp.request_id) ctx.throw(502, 'Something wrong with provider');

  ctx.body = {
    requestId: nexmoResp.request_id
  };
});

authRouter.post('/local/register', async ctx => {
  // ctx.body = ctx.request.url;

  const {
    email = '',
    phone = '',
    agreed = false,
    zip = '',
    code = '',
    password = '',
    businessName = ''
  } = ctx.request.body;

  // if (!agreed) return ctx.throw(409, 'Not agreed');

  if (!(await GeocodingService.checkEreaCondition(zip)))
    return ctx.throw(404, 'Service not awailable on this area', { zip });

  const fixedPhone = `1${phone}`;

  /* Check if account exist */
  let account = null;
  account = await Repairshop.findOne({ 'auth.local.phone': fixedPhone, 'auth.local.phone_verified': true });

  if (account) return ctx.throw(409, 'Phone number already exist', { exist: true, field: 'phone', phone: fixedPhone });

  const accountEmail = await Repairshop.findOne({ 'auth.local.email': email, 'auth.local.email.email_verified': true });
  if (accountEmail) return ctx.throw(409, 'Email already exist', { exist: true, field: 'email', email });

  const nexmoResp = { status: 0 };

  if (Number(nexmoResp.status) !== 0) ctx.throw(409, 'Code does not match');

  const repairshop = await Repairshop.create({
    'auth.local.phone': fixedPhone,
    'auth.local.email': email,
    'auth.local.phoneVerified': true,
    profile: {
      businessName
    }
    // 'auth.local.email_verified': true
  });

  const hashedPassword = await CryptService.generateHash(password);

  await repairshop.updateOne({ 'auth.local.password': hashedPassword });

  const issuedToken = await TokenService.generate({ id: repairshop.id, usertype: 'Repairshop' });

  ctx.body = issuedToken;
});

authRouter.post('/local/login', async ctx => {
  const { email = '', password = '' } = ctx.request.body;

  const account = await Repairshop.findOne({ 'auth.local.email': email }).select('auth.local.password');

  if (!account) return ctx.throw(422, 'Wrong login or password');

  const isEqual = await CryptService.compare(password, account.auth.local.password);
  if (!isEqual) return ctx.throw(422, 'Wrong login or password');

  const issuedToken = await TokenService.generate({ id: account.id, usertype: 'Repairshop' });

  ctx.body = issuedToken;
});

authRouter.get('/local/logout', Authentication, async ctx => {
  const { user } = ctx.state;
  await TokenService.destroy(user.rjwt);

  ctx.status = 201;
});
module.exports = authRouter.routes();
