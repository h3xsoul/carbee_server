const Router = require('koa-router');

const Chat = require('../../../models/chat');
const Message = require('../../../models/message');

const chatsRouter = new Router({
  prefix: '/chats'
});

chatsRouter.get('/', async ctx => {
  const { user } = ctx.state;

  const chats = await Chat.find({ repairshop: user.id })
    .populate('customer', 'profile')
    .lean()
    .exec();

  ctx.body = chats;
});

chatsRouter.get('/:chatId', async ctx => {
  const { user } = ctx.state;
  const { chatId } = ctx.params;

  const chat = await Chat.findOne({ _id: chatId, repairshop: user.id })
    .lean()
    .exec();

  if (!chat) return ctx.throw(404, 'chat not found');

  const messages = await Message.find({ chat: chat._id })
    .lean()
    .exec();

  ctx.body = messages;
});

// chatsRouter.get('/:chatId/messages', async ctx => {
//   const { user } = ctx.state;
//   const { chatId } = ctx.params;

//   const chat = await Chat.findOne({ _id: chatId, customer: user.id })
//   if (!chat) return ctx.throw(404, 'chat not found');

//   const m

//   const message = await Message.find({ chat: chat.id });

//   ctx.body = message;
// });

module.exports = chatsRouter.routes();
