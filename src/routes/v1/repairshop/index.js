const Router = require('koa-router');

/* Services */
const TokenService = require('../../../service/token.service');

/* Models */
const Repairshop = require('../../../models/repairshop');

/* Middlewares */
const Authentication = require('../../../middleware/auth.middleware');

const reprshopRouter = new Router({
  prefix: '/repairshop'
});

reprshopRouter.use(require('./auth'));

reprshopRouter.use(Authentication);

reprshopRouter.use(require('./quote'));

reprshopRouter.use(require('./testhook'));

reprshopRouter.use(require('./chats'));

reprshopRouter.get('/', async ctx => {
  const { user } = ctx.state;

  const account = await Repairshop.findById(user.id);

  if (!account) {
    await TokenService.destroy(user.rjwt);

    return ctx.throw('404', 'Customer not found');
  }

  ctx.body = account;
});

module.exports = reprshopRouter.routes();
