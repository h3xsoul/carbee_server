const Router = require('koa-router');
const socket = require('socket.io');
const path = require('path');
const etag = require('etag');
const fs = require('fs');

const publicDir = path.join(process.cwd(), 'public');

const router = new Router();

router.get('/ping', ctx => {
  ctx.body = 'pong';
});

router.get('/public/:file', async ctx => {
  const { file } = ctx.params;
  const filePath = path.join(publicDir, file);
  if (file.includes('/') || file.includes('..')) {
    ctx.throw(400, 'Nested paths are not allowed');
  }

  const etagBuffer = Buffer.from([ctx.originalUrl]);

  ctx.status = 200;
  ctx.set('ETag', etag(etagBuffer));

  if (ctx.fresh) {
    ctx.status = 304;
  } else {
    ctx.type = path.extname(file);
    const st = fs.createReadStream(filePath);
    st.on('close', () => st.destroy());
    ctx.body = st;
  }
});

router.use(require('./v1'));

module.exports = app => app.use(router.routes());
